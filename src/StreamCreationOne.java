import java.util.function.Consumer;
import java.util.stream.Stream;

public class StreamCreationOne {

	public static void main(String[] args) {
		
		Integer numbers[]= {10,20,30,40,50,60};
		Stream<Integer> stream=Stream.of(numbers);
		//Stream<Integer> stream=Stream.of(10,20,30,40,50,60,70,80,90,100);
		//Consumer<Integer> consumer1=(item)->System.out.println(item);
		//Consumer<Integer> consumer2=(item)->System.out.println(item*item);
		//stream.forEach(consumer1);
		//System.out.println("_____________________________________");
		//stream.forEach(consumer2);
		stream.forEach((item)->{if(item%20==0) System.out.println(item);});

	}

}
