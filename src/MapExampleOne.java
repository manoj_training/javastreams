import java.util.function.Function;
import java.util.stream.Stream;

public class MapExampleOne {

	public static String convert(String element) {
		char ch=(char)(((int)element.charAt(0))-32);
		StringBuffer sb=new StringBuffer(element);
		sb.setCharAt(0, ch);
		return sb.toString();
	}
	
	public static void main(String[] args) {
		
		
		Stream<String> names=Stream.of("rajeev","amit","suresh","ramesh","ashish","amlesh","priya","pinky");
		
		//Function<String,String> function=MapExampleOne::convert;
		//names.map(function).forEach(System.out::println);
		
		//names.map((element)->element.toUpperCase()).forEach(System.out::println);
		
		
		
	}

}
