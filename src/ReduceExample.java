import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

public class ReduceExample {
	public static void main(String[] args) {

		Stream<Integer> stream=Stream.of(1,2,3,4,5);
		
		BinaryOperator<Integer> operator=(n1,n2)->n1+n2;
		
		Optional<Integer> result=stream.reduce((n1,n2)->n1*n2);
		if(result.isPresent())
			System.out.println(result.get());
		else
			System.out.println("No Value Found");
	}
}
