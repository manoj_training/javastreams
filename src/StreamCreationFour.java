import java.util.HashMap;
import java.util.Map;

public class StreamCreationFour {

	public static void main(String[] args) {
		
		Map<String,Integer> scores=new HashMap<>();
		scores.put("virat",80); scores.put("rohit",90); scores.put("dhoni",99); scores.put("laxman",20);
		
	
		//scores.keySet().stream().forEach(System.out::println);

		//scores.values().stream().forEach(System.out::println);
		
		scores.entrySet().stream().forEach(System.out::println);
		
		
	}

}
