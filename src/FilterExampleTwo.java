import java.util.stream.Stream;

public class FilterExampleTwo {

	public static void main(String[] args) {
	
		Stream<String> names=Stream.of("rajeev","amit","suresh","ramesh","ashish","amlesh","priya","pinky");
		
		names.filter((name)->name.startsWith("r")).forEach(System.out::println);
		
		//long no=names.filter((name)->name.startsWith("r")).count();
		//System.out.println("Total "+no);

	}

}
