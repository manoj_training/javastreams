import java.util.Arrays;
import java.util.List;

public class EmpData {
	public static List<Employee> getEmpList(){
		
		List<Employee> employees=Arrays.asList(new Employee(1001,"Raj",50000,"SSE"), new Employee(1002,"Ashu",60000,"TL"), new Employee(1003,"Riya",80000,"TL"), new Employee(1001,"Priya",54000,"SE"), new Employee(1005,"Bhavesh",90000,"PM"), new Employee(1006,"Prem",30000,"SE"), new Employee(1007,"Disha",225000,"VP"));
		return employees;
	}
}
