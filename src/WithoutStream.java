import java.util.ArrayList;
import java.util.List;

public class WithoutStream {

	public static void main(String[] args) {
		
		List<Employee> employees=EmpData.getEmpList();
		
		
		//creating list of SE
		
		List<Employee> seList=new ArrayList<>();
		for(Employee employee:employees) {
			if(employee.getDesg().equals("SE"))
				seList.add(employee);
		}
		for(Employee employee:seList) {
			System.out.println(employee);
		}
		
		//creating list of SE earning less than 50000
		List<Employee> lowSalSEList=new ArrayList<>();
		for(Employee employee:seList) {
			if(employee.getSal()<50000)
				lowSalSEList.add(employee);
		}
		System.out.println("_____________________________________________________");

		for(Employee employee:lowSalSEList) {
			System.out.println(employee);
		}
		
		//computing avg sal of SE earning less that 50000
		int total=0;
		for(Employee employee:lowSalSEList) {
			total=total+employee.getSal();
		}
		System.out.println("_____________________________________________________");
		System.out.println("Avg Salary : "+total/lowSalSEList.size());
		System.out.println("_____________________________________________________");	
		
		
		

	}

}


/*
	1) we need to get a  separate list of all SE
	2) we want to filter out those SE who are earning less than 50000
	3) we wish to compute their avg salary (those who are earning less than 50000)
	
*/
