import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class SortedExample {

	public static void main(String[] args) {
	
		/*
		List<String> list=Arrays.asList("indore","delhi","mumbai","agra","chennai");
		Stream<String> stream=list.stream();
		stream.sorted().forEach(System.out::println);
		System.out.println("_____________________________________");
		list.forEach(System.out::println);
		*/
		
		List<Employee> employees=EmpData.getEmpList();
		employees.stream().sorted((e1,e2)->e1.getSal()-e2.getSal()).forEach(System.out::println);

	}

}
