import java.util.Random;
import java.util.stream.Stream;

public class StreamCreationThree {

	public static void main(String[] args) {
		
		Stream<Integer> randomNumbers=Stream.generate(()->(new Random()).nextInt(100));
		Stream<Integer> twentyRandomNumbers=randomNumbers.limit(20);
		twentyRandomNumbers.forEach(System.out::println);
		System.out.println("_________________________________________");
		
		Stream.generate(()->new Random().nextInt(50)).limit(5).forEach(System.out::println);
		
		
	}

}
