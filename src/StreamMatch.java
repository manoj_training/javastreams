import java.util.List;

public class StreamMatch {

	public static void main(String[] args) {
		
		List<Employee> employees=EmpData.getEmpList();
		//boolean result=employees.stream().anyMatch((employee)->employee.getDesg().equals("VP"));
		//boolean result=employees.stream().allMatch((employee)->employee.getSal()>=40000);
		boolean result=employees.stream().noneMatch((employee)->employee.getSal()>=200000);
		System.out.println(result);
	}

}
