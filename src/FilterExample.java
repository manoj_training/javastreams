import java.util.function.Predicate;
import java.util.stream.Stream;

public class FilterExample {

	public static boolean isEven(int number) {
		return number%2==0;
		
	}

	public static void main(String[] args) {

		
		Stream<Integer> stream=Stream.of(10,15,22,45,34,89,133,140,200,205);
		
		//Predicate<Integer> predicate=FilterExample::isEven;
		
		//Stream<Integer> evenNumberStream=stream.filter(predicate);
		
		//Stream<Integer> evenNumberStream=stream.filter((number)->number%2==0);
		//evenNumberStream.forEach(System.out::println);
		//long n=evenNumberStream.count();
		//System.out.println(n);
		
		long n=stream.filter((number)->number%2==0).count();
		System.out.println("Total Even Numbers : "+n);
	}

}
