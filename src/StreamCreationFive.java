import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamCreationFive {

	public static void main(String[] args) {
		String city="INDORE";
		IntStream stream=city.chars();
		stream.forEach((n)->{System.out.println((char)n);});
		System.out.println("__________________________________________________");
		String subjects="java,mysql,python";
		
		Stream.of(subjects.split(",")).forEach(System.out::println);		
	}

}
