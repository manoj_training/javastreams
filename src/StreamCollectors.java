import java.util.List;
import java.util.stream.Collectors;

public class StreamCollectors {

	public static void main(String[] args) {
	
		List<Employee> employees=EmpData.getEmpList();
		employees.forEach(System.out::println);
		System.out.println("_____________________________________________________");
		List<Employee> seList=employees.stream().filter((employee)->{return employee.getDesg().equals("SE");}).collect(Collectors.toList());
		System.out.println("_____________________________________________________");
		employees.forEach(System.out::println);
		System.out.println("_____________________________________________________");
		seList.forEach(System.out::println);
		

	}

}
