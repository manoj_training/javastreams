import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StreamCreationTwo {

	public static void main(String[] args) {
	
		List<String> cities=Arrays.asList("dewas","ujjain","delhi","mumbai");
		Stream<String> stream=cities.stream();
		//stream.forEach((city)->{System.out.println(city);});
		stream.forEach(System.out::println);
		System.out.println("__________________________________________________________________________");
		List<Employee> employees=EmpData.getEmpList();
		Stream<Employee> empStream=employees.stream();
		empStream.forEach((employee)->{if(employee.getDesg().equals("TL"))
			System.out.println(employee);
		});
	}

}
