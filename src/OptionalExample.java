import java.util.Optional;

public class OptionalExample {

	private static String city="bhopal";
	
	public static void printInUpperCase(Optional<String> optional) {

		if(optional.isPresent()) {
			String s=optional.get();
			System.out.println(s.toUpperCase());
		}else {
			System.out.println("Please pass a valid value..! (no nulls allowed.)");
		}
		
	}
	
	public static void main(String[] args) {
		
		
		Optional<String> optional=Optional.ofNullable(city);
		printInUpperCase(optional);
		
	}
}
