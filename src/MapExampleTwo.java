import java.util.stream.Stream;

public class MapExampleTwo {

	public static void main(String[] args) {
		
		EmpData.getEmpList().forEach(System.out::println);
		System.out.println("_______________________________________________________________________");
		Stream<Employee> stream=EmpData.getEmpList().stream();
		stream.map((employee)->{employee.setSal(employee.getSal()+10000);return employee;}).filter((employee)->employee.getSal()>=50000).forEach(System.out::println);
		System.out.println("_______________________________________________________________________");
		EmpData.getEmpList().forEach(System.out::println);
		System.out.println("_______________________________________________________________________");
	
		

	}

}

/*
               wish to give an increment of 10000 to every employee
               and print those employees whose income is taxable after increment (taxable income>=50000)
 
 
*/